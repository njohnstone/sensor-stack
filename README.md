# Sensor Stack
Creates a test environment that collects time series data and visualises it via Grafana

## Contents
    .
    ├── README.md             # Doco
    ├── docker-compose.yml    # docker compose configuration
    └── .env                  # environment variables

## Setup
- Install docker and docker-compose (https://docs.docker.com/compose/install/)
- Open .env and customise any values you like
- Open terminal / Powershell and run `docker compose up`
- Check you can reach InfluxDB (http://localhost:8086)
- Check you can reach Grafana (http://localhost:3000)

## Tear down
- Open terminal / Powershell and run `docker compose down`

## InfluxDB (Data collection)
**Optional** Use whatever database you want

## Grafana (Data visualisation)
- Login to grafana
- Go to Settings -> Datasources and setup your database
- Create a Dashboard
